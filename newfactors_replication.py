"""
Author: Lira Mota
Date: 2019-02
Code: Replicates Fama and French 5 Factor

Achieved Correlations:
SMB: 0.998269
HML: 0.996111
RMW: 0.987859
CMA: 0.982491
"""

# %% Packages

import pandas as pd
from pylab import *
from matplotlib.pyplot import figure

from utils.monthly_date import *

from portools.find_breakpoints import find_breakpoints
from portools.sort_portfolios import sort_portfolios

desired_width = 10
pd.set_option('display.width', desired_width)
idx = pd.IndexSlice

# %% Set Up

char_breakpoints = {'me': [0.5],
                    'beme': [0.3, 0.7],
                    'opbe': [0.3, 0.7],
                    'inv': [0.3, 0.7]}

weightvar = 'melag_weights'

retvar = 'retadj'

dict_factors = {'beme': 'hml',
                'me': 'smb',
                'opbe': 'rmw',
                'inv': 'cma'}

# %% Download Data
# FF five factor
ff = pd.read_csv('../ff_factors.CSV')
ff.drop(columns=['date'], inplace=True)

# Annual Data
mdata = pd.read_pickle('../stock_monthly.pkl')
adata = pd.read_pickle('../stock_annual.pkl')

# Set names
adata.rename(columns={'mesum_june': 'me', 'inv_gvkey': 'inv'}, inplace=True) #inv_permco
mdata.rename(columns={weightvar: 'weightvar', retvar: 'retvar'}, inplace=True)

# %%
import wrds
def calculate_cumulative_returns(mdata, tt, min_periods): #TODO: to be completed
    """
    Calculate past returns for momentum stratagy

    Parameters:
    ------------
    mdata: data frame
        crsp monthly data with cols permno, date as index.
    tt: int
        number of periods to cumulate retuns
    min_periods: int
        minimum number of periods. Default tt/2
    """
    start_time = time.time()

    required_cols = ['retadj', 'ret']

    assert set(required_cols).issubset(mdata.columns), "Required columns: {}.".format(', '.join(required_cols))

    df = mdata[required_cols].copy()
    df['retadj'] = df['retadj']+1
    df['ret'] = df['ret'].notnull()

    df.reset_index(level=0, inplace=True)

    # Cumulative Return (adjusted) in 11 months
    cret = df.groupby('permno')['retadj'].rolling(window=tt, min_periods=1).apply(np.prod)
    nona_count = df.groupby('permno')['ret'].rolling(window=tt, min_periods=1).apply(np.sum)
    
    cret = cret-1
    cret = cret.to_frame('ret' + str(tt))
    cret[nona_count <= min_periods] = np.nan

    print("Time to calculate %d months past returns: %s seconds" % (tt, str(round(time.time() - start_time, 2))))

    return cret
db = wrds.Connection(wrds_username='sagar520')  # make sure to configure wrds connector before hand.

# %% 
mdata['closeness'] = calculate_cumulative_returns(mdata, 11, 6)
# %% Create Filters
# Global Filters: applied to all portfolio sorts

# valid return
# ------------
print('Data deleted due to missing return: %f' % np.round(mdata.retvar.isnull().mean()*100, 2))
rdata = mdata[mdata.retvar.notnull()]

# shrcd must be (10,11)
# ---------------------
print('Data deleted due to shrcd: %f' % np.round((1-adata.shrcd.isin([10, 11]).mean())*100, 2))
sort_data = adata[adata.shrcd.isin([10, 11])].copy()

# exchcd must be (1, 2, 3)
# ------------------------
print('Data deleted due to exchcd: %f' % np.round((1-sort_data.exchcd.isin([1, 2, 3]).mean())*100, 2))
sort_data = sort_data[sort_data.exchcd.isin([1, 2, 3])]

#del adata, mdata

# Portfolio Specif Filters
sortvars = [*char_breakpoints]

# Define the break point filters
# ------------------------------
bp_filters = [None] * len(sortvars)
bp_filters = dict(zip(sortvars, bp_filters))

# Define the sample filter
# ------------------------
sample_filters = [None] * len(sortvars)
sample_filters = dict(zip(sortvars, sample_filters))

for sortvar in sortvars:
    # notice that the way we defined beme or beme is null if be<=0
    bp_filters[sortvar] = (sort_data[sortvar].notnull())
    sample_filters[sortvar] = ((sort_data.me > 0) & (sort_data.mesum_dec > 0) & (sort_data[sortvar].notnull()))

# %% Find Breakpoints
# The number of firms Ken French reports for each characteristic sort varies. This means his universe of stocks
# for sorting changes across characteristics. That is why we need to calculate breakpoints separately.
breakpoints = [None] * len(sortvars)
breakpoints = dict(zip(sortvars, bp_filters))

for sortvar in sortvars:
    breakpoints[sortvar] = find_breakpoints(data=sort_data[bp_filters[sortvar]],
                                            quantiles={sortvar: char_breakpoints[sortvar]},
                                            id_variables=['rankyear', 'permno', 'exchcd'],
                                            exch_cd=[1]
                                            )

# %% Portfolio Sorts
portsorts = [None] * len(sortvars)
portsorts = dict(zip(sortvars, bp_filters))

for sortvar in sortvars:
    portsorts[sortvar] = sort_portfolios(data=sort_data[sample_filters[sortvar]],
                                         quantiles={sortvar: char_breakpoints[sortvar]},
                                         id_variables=['rankyear', 'permno', 'exchcd'],
                                         breakpoints={sortvar: breakpoints[sortvar]}
                                         )

# merge all separate portfolio allocations together
port = portsorts['me'].copy()
for sortvar in sortvars[1:]:
    # Notice that this is an outer join
    port = port.merge(portsorts[sortvar], on=['permno', 'rankyear'], how='outer')

# %% Calculate rertuns
# Notice that this is a inner join
port_ret = pd.merge(port, rdata, on=['rankyear', 'permno'])

# Characteristic: beme
# --------------------
charvars = ['me', 'beme']
beme_ret = port_ret.groupby(['date']+[charvar+"portfolio" for charvar in charvars]).apply(lambda x: np.average(x.retvar, weights=x.weightvar))
beme_ret = beme_ret.unstack(level=[1, 2])
beme_ret['hml'] = beme_ret.loc[:, idx[:, 'beme3']].mean(axis=1) - beme_ret.loc[:, idx[:, 'beme1']].mean(axis=1)
del charvars

# Characteristic: op
# -------------------
charvars = ['me', 'opbe']
op_ret = port_ret.groupby(['date']+[charvar+"portfolio" for charvar in charvars]).apply(lambda x: np.average(x.retvar, weights=x.weightvar))
op_ret = op_ret.unstack(level=[1, 2])
op_ret['rmw'] = op_ret.loc[:, idx[:, 'opbe3']].mean(axis=1) - op_ret.loc[:, idx[:, 'opbe1']].mean(axis=1)
del charvars

# Characteristic: inv
# -------------------
charvars = ['me', 'inv']
inv_ret = port_ret.groupby(['date']+[charvar+"portfolio" for charvar in charvars]).apply(lambda x: np.average(x.retvar, weights=x.weightvar))
inv_ret = inv_ret.unstack(level=[1, 2])
inv_ret['cma'] = inv_ret.loc[:, idx[:, 'inv1']].mean(axis=1) - inv_ret.loc[:, idx[:, 'inv3']].mean(axis=1)
del charvars

# Characteristic: size
# --------------------
fret = pd.DataFrame()
fret['smb'] = (beme_ret.loc[:, 'me1'].mean(axis=1) - beme_ret.loc[:,'me2'].mean(axis=1) +
               op_ret.loc[:, 'me1'].mean(axis=1) - op_ret.loc[:,'me2'].mean(axis=1) +
               inv_ret.loc[:, 'me1'].mean(axis=1) - inv_ret.loc[:,'me2'].mean(axis=1))*(1/3)

# %% Aggregate all
beme_ret.columns = [''.join(col).strip() for col in beme_ret.columns.values]
op_ret.columns = [''.join(col).strip() for col in op_ret.columns.values]
inv_ret.columns = [''.join(col).strip() for col in inv_ret.columns.values]

fret = fret.join(beme_ret[['hml']]).join(op_ret[['rmw']]).join(inv_ret[['cma']])
fret.reset_index(inplace=True)
fret['mdate'] = fret.date.apply(monthly_date)

fdata = pd.merge(fret, ff_ret, on='mdate')
fdata.drop(columns=['mdate'], inplace=True)
fdata.set_index('date', inplace=True)

# %% Checks
sf = ['smb', 'hml', 'rmw', 'cma', 'SMB', 'HML', 'RMW', 'CMA']
fdata[sf].corr()[0:4][['SMB', 'HML', 'RMW', 'CMA']]

# 1$ invested returns
plot_data = (1+fdata[sf].add(fdata['RF'],  axis='index')).cumprod()
plot_data.plot(grid=True, logy=True)
plot_data[['smb', 'SMB']].plot(grid=True, logy=True)
plot_data[['hml', 'HML']].plot(grid=True, logy=True)
plot_data[['rmw', 'RMW']].plot(grid=True, logy=True)
plot_data[['cma', 'CMA']].plot(grid=True, logy=True)




