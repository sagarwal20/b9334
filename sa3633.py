# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 12:37:54 2019

@author: dell
"""
import pandas as pd
import wrds
import time
char_breakpoints = {'me': [0.5],
                    'beme': [0.3, 0.7],
                    'op': [0.3, 0.7],
                    'inv': [0.3, 0.7]}

weightvar = 'melag'

retvar = 'retadj'

rankvar = 'rankyear'

dict_factors = {'BEME': 'HML',
                'ME': 'SMB',
                'OP': 'RMW',
                'INV': 'CMA'}

mdata = pd.read_pickle('stock_monthly.pkl')
adata = pd.read_pickle('stock_annual.pkl')

adata.rename(columns= {'mesum_june':'me', 'inv_qvkey':'inv'}, inplace = True)
mdata.rename(columns= {weightvar:'weightvar', retvar:'retvar'}, inplace = True)
## Risk-free Rate
db = wrds.Connection(wrds_username='sagar520')  # make sure to configure wrds connector before hand.
mdata['retadj'] = mdata['retvar']
mdata['ret_11_1'] = calculate_cumulative_returns(mdata, 11, 6)

query = "SELECT caldt as date, t30ret as rf FROM crspq.mcti"
rf = db.raw_sql(query, date_cols=['date'])
rf.set_index('date', inplace=True)
#####valid returns
rdata =  mdata[mdata.retvar.notnull()]

### shrd check

sort_data = adata[adata.shrcd.isin([10,11])].copy()


###exch check

sort_data = sort_data[sort_data.exchcd.isin([1,2,3])]


### breakpoint
sortvars = [*char_breakpoints]

bp_filters = [None] * len(sortvars)
bp_filters = dict(zip(sortvars, bp_filters))

sample_filters = [None] * len(sortvars)
sample_filters = dict(zip(sortvars, sample_filters))

for sortvar in sortvars:
    # for each sortvar we define filters according to the exchanges that should be taken into account (e.g. FF only use NYSE firms)
    # notice that the way we defined BEME or OP: NULL if BE<=0
    bp_filters[sortvar] = ((sort_data[sortvar].notnull()))
    sample_filters[sortvar] = ((sort_data.me > 0) & (sort_data.mesum_dec > 0) &(sort_data[sortvar].notnull()))


bps = [None] * len(sortvars)
bps = dict(zip(sortvars, bps))
from fire_pytools.portools.find_breakpoints import *

for sortvar in sortvars:
    bps[sortvar] = find_breakpoints(sort_data[bp_filters[sortvar]],
                                      quantiles={sortvar: char_breakpoints[sortvar]},
                                      id_variables=[rankvar, 'permno', 'exchcd'], exch_cd = [1])

# Sort portfolios
ports = [None] * len(sortvars)
ports = dict(zip(sortvars, bp_filters))

from fire_pytools.portools.sort_portfolios import *

# calculate the portfolio allocation for each characteristic and save them in a list
for sortvar in sortvars:
    ports[sortvar] = sort_portfolios(sort_data[sample_filters[sortvar]],
                                       quantiles={sortvar: char_breakpoints[sortvar]},
                                       id_variables=[rankvar, 'permno', 'exchcd'],
                                       breakpoints={sortvar: bps[sortvar]})



#### merge
mergedp = ports['me'].copy()
for sortvar in sortvars[1:]:
    mergedp = mergedp.merge(ports[sortvar], on=['permno', rankvar], how='outer')


port_ret = pd.merge(mergedp, rdata, on=['rankyear', 'permno'])

idx = pd.IndexSlice
def wavg(group, avg_name, weight_name):
    """ http://stackoverflow.com/questions/10951341/pandas-dataframe-aggregate-function-using-multiple-columns
    In rare instance, we may not have weights, so just return the mean. Customize this if your business case
    should return otherwise.
    """
    d = group[avg_name]
    w = group[weight_name]
    try:
        return (d * w).sum() / w.sum()
    except ZeroDivisionError:
        return d.mean()

#### Factors
# BEME
charvars = ['me','beme']
beme_ret = port_ret.groupby(['date']+[charvar+"portfolio" for charvar in charvars]).apply(wavg, 'retvar', 'melag')
beme_ret = beme_ret.unstack(level=[1,2])
beme_ret['HML'] = beme_ret.loc[:,idx[:,'beme3']].mean(axis=1) - beme_ret.loc[:,idx[:,'beme1']].mean(axis=1)
beme_ret.columns =[''.join(col).strip() for col in beme_ret.columns.values]

# OP
charvars = ['me','op']
op_ret = port_ret.groupby(['date']+[charvar+"portfolio" for charvar in charvars]).apply(wavg, 'retvar', 'melag')
op_ret = op_ret.unstack(level=[1,2])
op_ret['RMW'] = op_ret.loc[:,idx[:,'op3']].mean(axis=1) - op_ret.loc[:,idx[:,'op1']].mean(axis=1)
op_ret.columns =[''.join(col).strip() for col in op_ret.columns.values]

# INV
charvars = ['me','inv']
inv_ret = port_ret.groupby(['date']+[charvar+"portfolio" for charvar in charvars]).apply(wavg, 'retvar', 'melag')
inv_ret = inv_ret.unstack(level=[1,2])
inv_ret['CMA'] = inv_ret.loc[:,idx[:,'inv1']].mean(axis=1) - inv_ret.loc[:,idx[:,'inv3']].mean(axis=1)
inv_ret.columns =[''.join(col).strip() for col in inv_ret.columns.values]

# SMB
charvars = ['inv', 'me']
smbinv_ret = port_ret.groupby(['date']+[charvar+"portfolio" for charvar in charvars]).apply(wavg, 'retvar', 'melag')
smbinv_ret = smbinv_ret.unstack(level=[1,2])
smbinv_ret['sbm_i'] = smbinv_ret.loc[:,idx[:,'me1']].mean(axis=1) - smbinv_ret.loc[:,idx[:,'me2']].mean(axis=1)
smbinv_ret.columns =[''.join(col).strip() for col in smbinv_ret.columns.values]

charvars = ['op', 'me']
smbop_ret = port_ret.groupby(['date']+[charvar+"portfolio" for charvar in charvars]).apply(wavg, 'retvar', 'melag')
smbop_ret = smbop_ret.unstack(level=[1,2])
smbop_ret['sbm_o'] = smbop_ret.loc[:,idx[:,'me1']].mean(axis=1) - smbop_ret.loc[:,idx[:,'me2']].mean(axis=1)
smbop_ret.columns =[''.join(col).strip() for col in smbop_ret.columns.values]

charvars = ['beme', 'me']
smbbe_ret = port_ret.groupby(['date']+[charvar+"portfolio" for charvar in charvars]).apply(wavg, 'retvar', 'melag')
smbbe_ret = smbbe_ret.unstack(level=[1,2])
smbbe_ret['sbm_b'] = smbbe_ret.loc[:,idx[:,'me1']].mean(axis=1) - smbbe_ret.loc[:,idx[:,'me2']].mean(axis=1)
smbbe_ret.columns =[''.join(col).strip() for col in smbbe_ret.columns.values]

smb = (smbinv_ret['sbm_i'] +smbop_ret['sbm_o'] + smbbe_ret['sbm_b'])/3

# MOM############################
rdata =  mdata[mdata.retvar.notnull()]

msort_data = rdata[rdata.shrcd.isin([10,11])].copy()
msort_data = msort_data[msort_data.exchcd.isin([1,2,3])]
sortvars = ['ret_11_1', 'me']

bp_filters = [None] * len(sortvars)
bp_filters = dict(zip(sortvars, bp_filters))

sample_filters = [None] * len(sortvars)
sample_filters = dict(zip(sortvars, sample_filters))

for sortvar in sortvars:
    # for each sortvar we define filters according to the exchanges that should be taken into account (e.g. FF only use NYSE firms)
    # notice that the way we defined BEME or OP: NULL if BE<=0
    bp_filters[sortvar] = ((msort_data[sortvar].notnull()))
    sample_filters[sortvar] = ((msort_data.me > 0) &(msort_data[sortvar].notnull()))

bps = [None] * len(sortvars)
bps = dict(zip(sortvars, bps))

bps['ret_11_1'] = find_breakpoints(msort_data[bp_filters['ret_11_1']],
                                  quantiles={'ret_11_1': [0.3, 0.7]},
                                  id_variables=[rankvar, 'permno', 'exchcd'], exch_cd = [1])
bps['me'] = find_breakpoints(msort_data[bp_filters['me']],
                                  quantiles={'me': [0.5]},
                                  id_variables=[rankvar, 'permno', 'exchcd'], exch_cd = [1])

# Sort portfolios
ports = [None] * len(sortvars)
ports = dict(zip(sortvars, bp_filters))

# calculate the portfolio allocation for each characteristic and save them in a list
ports['me'] = sort_portfolios(msort_data[sample_filters['me']],
                                   quantiles={'me': [0.5]},
                                   id_variables=[rankvar, 'permno', 'exchcd'],
                                   breakpoints={'me': bps['me']})
ports['ret_11_1'] = sort_portfolios(msort_data[sample_filters['ret_11_1']],
                                   quantiles={'ret_11_1': [0.3, 0.7]},
                                   id_variables=[rankvar, 'permno', 'exchcd'],
                                   breakpoints={'ret_11_1': bps['ret_11_1']})

#### merge
mergedp = ports['ret_11_1'].copy()
mergedp = mergedp.merge(ports['me'], on=['permno', rankvar], how='outer')

port_ret = pd.merge(mergedp, rdata, on=['rankyear', 'permno'])
# MOM
charvars = ['me','ret_11_1']
mom_ret = port_ret.groupby(['date']+[charvar+"portfolio" for charvar in charvars]).apply(wavg, 'retvar', 'melag')
mom_ret = mom_ret.unstack(level=[1,2])
mom_ret['MOM'] = mom_ret.loc[:,idx[:,'ret_11_13']].mean(axis=1) - mom_ret.loc[:,idx[:,'ret_11_11']].mean(axis=1)
mom_ret.columns =[''.join(col).strip() for col in mom_ret.columns.values]



#### Merging Datasets
fdata = pd.merge(rf, beme_ret, on=['date'])
fdata = pd.merge(fdata, op_ret, on=['date'])
fdata = pd.merge(fdata, inv_ret, on=['date'])
fdata = pd.merge(fdata, smbinv_ret, on=['date'])
fdata = pd.merge(fdata, smbop_ret, on=['date'])
fdata = pd.merge(fdata, smbbe_ret, on=['date'])
fdata['smb'] = (fdata['sbm_i'] +fdata['sbm_o'] + fdata['sbm_b'])/3
fdata = pd.merge(fdata, mom_ret, on=['date'])

fdata_f= fdata[['rf','HML','RMW','CMA','smb', 'MOM']]
fdata.reset_index(inplace = True)
import numpy as np

np.log(1+fdata_f[['HML']]).cumsum().plot()
np.log(1+fdata_f[['RMW']]).cumsum().plot()
np.log(1+fdata_f[['CMA']]).cumsum().plot()
np.log(1+fdata_f[['rf']]).cumsum().plot()
np.log(1+fdata_f[['smb']]).cumsum().plot()
np.log(1+fdata_f[['MOM']]).cumsum().plot()

#### correlations
ff_factors = pd.read_csv('ff_factors.csv')
import datetime
def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + datetime.timedelta(days=4)  # this will never fail
    return next_month - datetime.timedelta(days=next_month.day)

# you could also import date instead of datetime and use that.
ff_factors['date'] = ff_factors['date'].apply(lambda x: last_day_of_month(pd.to_datetime(str(x), format='%Y%m')))
ff_factors.set_index('date', inplace=True)

ff_factors = pd.merge(fdata_f, ff_factors, on=['date'])
np.corrcoef(ff_factors['RF_t'],ff_factors['rf'])
np.corrcoef(ff_factors['HML_t'],ff_factors['HML'])
np.corrcoef(ff_factors['RMW_t'],ff_factors['RMW'])
np.corrcoef(ff_factors['CMA_t'],ff_factors['CMA'])
np.corrcoef(ff_factors['SMB_t'],ff_factors['smb'])
np.log(1+ff_factors[['RMW_t']]).cumsum().plot()
np.log(1+ff_factors[['Mkt-RF']]).cumsum().plot()
